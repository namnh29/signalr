﻿using System.Data.Entity;

namespace MVCRealtimeSignalR.Models
{
    public class SignalRDbContext : DbContext
    {
        public SignalRDbContext() : base("name=SignalRDbContext")
        {
        }

        public DbSet<Employee> Employees { get; set; }
    }
}
