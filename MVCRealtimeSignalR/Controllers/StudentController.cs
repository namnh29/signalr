﻿using MVCRealtimeSignalR.Hubs;
using MVCRealtimeSignalR.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCRealtimeSignalR.Controllers
{
    public class StudentController : Controller
    {
        // GET: Student
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetAllStudent()
        {
            try
            {
                List<STUDENT> data = new List<STUDENT>();
                using (DBDataContext SQL = new DBDataContext())
                {
                    data = SQL.STUDENTs.ToList();
                }
                return Json(new { Status = true, Data = data, Message = "OK" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Status = false, Message = "False cmnr" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult CreateStudent(STUDENT data)
        {
            try
            {
                using (DBDataContext SQL = new DBDataContext())
                {
                    SQL.STUDENTs.InsertOnSubmit(data);
                    SQL.SubmitChanges();
                }
                EmployeesHub.BroadcastData();
                return Json(new { Status = true, Data = data, Message = "OK" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Status = false, Message = "False cmnr" }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}